package Core;

import java.util.ArrayList;
import java.util.HashMap;

public class Inventario {
	
	/**
	 * Hashmap with all the items in inventory.
	 */
	static HashMap<Item, Integer> inventory = new HashMap<Item, Integer>();
	
	/**
	 * Method used to put an item in the inventory.
	 */
	public static void newItem(Item obj, int n) {
		
		if(Item.itemList.contains(obj)) {
			
			if(inventory.containsKey(obj)) {
				inventory.put(obj, inventory.get(obj)+n);
			}else {
				inventory.put(obj, n);
			}
			
		}
		
	}
	
}
