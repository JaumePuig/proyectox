package Core;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Combate {
	
	static boolean atrapado;
	
	/**
	 * This method initiates a battle with a wild pokemon.
	 * @param allyHp
	 * @param nidorino Team used by the character.
	 * @param t Board used.
	 * @param f Window used.
	 * @param alive boolean that will tell if character's pokemon are dead.
	 * @param mapa 
	 * @param pokeGratis 
	 * @param team 
	 * @param lider 
	 * @param vencidos 
	 * @return int character's pokemon hp.
	 * @throws IOException 
	 * 
	 */
	public static int combatInit(Board t, Window f, int allyHp, Team nidorino, boolean alive, int mapa, Boolean pokeGratis, ArrayList<Team> team, int lider) throws InterruptedException, IOException{
		f.playMusic("wildBattle.wav");
		atrapado = false;
		System.out.println(lider);
		allyHp = combate(allyHp, t, f, alive, mapa, pokeGratis, team, lider);
		
		return allyHp;
	}
	
	/**
	 * This method starts a battle with a wild pokemon.
	 * @param allyHp
	 * @param nidorino Team used by the character.
	 * @param t Board used.
	 * @param f Window used.
	 * @param alive boolean that will tell if character's pokemon are dead.
	 * @param mapa 
	 * @param pokeGratis 
	 * @param team 
	 * @param lider 
	 * @param vencidos 
	 * @return int character's pokemon hp.
	 * @throws IOException 
	 * 
	 */
	private static int combate(int allyHp, Board t, Window f, boolean alive, int mapa, Boolean pokeGratis, ArrayList<Team> team, int lider) throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		System.out.println("combate");
		Team nidorino = JuegoPokemon.team.get(lider);
		nidorino.hp = allyHp;
		Random r = new Random();
		int a = r.nextInt(3);
		
		
		
		Salvaje encuentro;
		
		if (mapa==2) {
			encuentro = new Salvaje(Salvaje.lista[a].maxhp, Salvaje.lista[a].hp, Salvaje.lista[a].atk,
					Salvaje.lista[a].def, Salvaje.lista[a].satk, Salvaje.lista[a].sdef, Salvaje.lista[a].spd,
					Salvaje.lista[a].name, Salvaje.lista[a].path, a);
			System.out.println("AAAAAAAAAAAAAAAAAAHHH");
		}else {
			System.out.println(Salvaje.listaEasy[a].name);
			encuentro = new Salvaje(Salvaje.listaEasy[a].maxhp, Salvaje.listaEasy[a].hp, Salvaje.listaEasy[a].atk,
					Salvaje.listaEasy[a].def, Salvaje.listaEasy[a].satk, Salvaje.listaEasy[a].sdef, Salvaje.listaEasy[a].spd,
					Salvaje.listaEasy[a].name, Salvaje.listaEasy[a].path, a);
			System.out.println(encuentro);
			System.out.println("BBBBBBBBBBBBBBB");
		}
		System.out.println(mapa);
		System.out.println(a);
		System.out.println(Salvaje.listaEasy[a]);
		System.out.println(encuentro);
		
		/*gengar.hp=100;*/
		
		Thread.sleep(500);

		String[] combates = { "", nidorino.path, encuentro.path, "pokeBall.gif" };

		t.setSprites(combates);
		t.setActimgbackground(true);
		t.setImgbackground("combate.png");

		int genvida = encuentro.hp;

		boolean turno = true;

		t.setActimgbackground(true);
		t.setImgbackground("combate.png");

		String encuentroHp = "     HP: " + encuentro.hp + "/" + encuentro.maxhp;
		String nidohp = "     HP: " + allyHp + "/" + nidorino.maxhp;

		int[][] vacio = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
				{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

		BoardSprite s = new BoardSprite("TextBox", 0, 0, 2, 2, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 1, 1, 1, 1, encuentroHp);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt2.text = true;
		BoardSprite s3 = new BoardSprite("TextBox", 3, 2, 5, 3, "textbox.png");
		BoardSprite nidhp = new BoardSprite("texto", 4, 2, 4, 2, nidohp);
		nidhp.font = new Font("Comic Sans MS", Font.BOLD, 12);
		nidhp.text = true;
		ArrayList<BoardSprite> hps = new ArrayList<>();
		hps.add(s);
		hps.add(txt2);
		hps.add(s3);
		hps.add(nidhp);

		t.setBoardSprites(hps);
		int[][][] cosa = { vacio, mapas.combate, vacio };
		char[] tipocosa = { 's', 's', 's' };
		t.draw(cosa, tipocosa);

		while (nidorino.hp > 0 && encuentro.hp > 0 && !atrapado) {

			encuentroHp = "     HP: " + encuentro.hp + "/" + encuentro.maxhp;
			nidohp = "     HP: " + nidorino.hp + "/" + nidorino.maxhp;

			if (turno) {

				turno = nidottack(turno, encuentroHp, nidohp, genvida, encuentro, nidorino, t, f, pokeGratis);

				Thread.sleep(200);

				System.out.println(turno);

			} else {

				System.out.println(encuentro.name+" ataca");
				turno = gengattack(turno, vacio, encuentro, nidorino, t, f);

				Thread.sleep(200);
			}

		}
		if (atrapado) {
			
			Team nuevo = new Team(encuentro.maxhp, encuentro.hp, encuentro.atk, encuentro.def, encuentro.satk, encuentro.sdef, encuentro.spd,
					1, 0, encuentro.name, encuentro.path2, encuentro.path, encuentro.move1.name, encuentro.move2.name, encuentro.move3.name, encuentro.move4.name);
			JuegoPokemon.team.add(nuevo);
			JuegoPokemon.HPs.add(nuevo.maxhp);
			System.out.println("Atrapado");
			for (int i = 0; i < JuegoPokemon.team.size(); i++) {
				System.out.println(JuegoPokemon.team.get(i));
			}
			JuegoPokemon.team.get(lider).atk=nidorino.atk;
			JuegoPokemon.team.get(lider).maxhp=nidorino.maxhp;
			JuegoPokemon.team.get(lider).hp=nidorino.hp;
			JuegoPokemon.team.get(lider).def=nidorino.def;
			JuegoPokemon.team.get(lider).satk=nidorino.satk;
			JuegoPokemon.team.get(lider).sdef=nidorino.sdef;
			JuegoPokemon.team.get(lider).spd=nidorino.spd;
			JuegoPokemon.team.get(lider).exp=nidorino.exp;
			JuegoPokemon.team.get(lider).lvl=nidorino.lvl;
			return nidorino.hp;
		}
		if (nidorino.hp > 0) {
			hps.clear();
			System.out.println(nidorino.hp);
			nidorino.exp+=30;
			nidorino.lvlUp();
			JuegoPokemon.vencidos++;
			return nidorino.hp;
		} else {
			System.out.println("YOU DIED");
			alive = false;
			hps.clear();
			derrota();
			return 0;
		}

	}
	
	/**
	 * This is the method that decides randomly the wild enemy pokemon move
	 * 
	 * @param turno boolean that will be in the return and shows which pokemon
	 *              attacks.
	 * 
	 * @param vacio int matrix used by the textboxes.
	 * @param encuentro Salvaje that the character will fight.
	 * @param nidorino Pokemon used by the character.
	 * @param t Board used.
	 * @param f Window used.
	 */
	
	private static boolean gengattack(boolean turno, int[][] vacio, Salvaje encuentro, Team nidorino, Board t, Window f) throws InterruptedException {
		// TODO Auto-generated method stub

		Random r = new Random();
		System.out.println("AAAAAAAAAAAAAAA");
		Thread.sleep(300);
		int ran = r.nextInt(4);

		switch (ran) {
		case 0:

			nidorino.hp -= ((encuentro.atk * encuentro.move1.damage) - (nidorino.def/3));
			movimiento('z', vacio, encuentro, t, f, nidorino);

			break;
		case 1:

			nidorino.hp -= ((encuentro.atk * encuentro.move2.damage) - (nidorino.def/3));
			movimiento('x', vacio, encuentro, t, f, nidorino);

			break;
		case 2:

			nidorino.hp -= ((encuentro.atk * encuentro.move3.damage) - (nidorino.def/3));
			movimiento('c', vacio, encuentro, t, f, nidorino);

			break;

		default:

			nidorino.hp -= ((encuentro.atk * encuentro.move4.damage) - (nidorino.def/3));
			movimiento('v', vacio, encuentro, t, f, nidorino);

			break;
		}

		return !turno;
	}

	/**
	 * This is the method for the player to decide nidorino's next attack
	 * 
	 * @param turno    boolean that will be in the return and shows which pokemon
	 *                 attacks.
	 * 
	 * @param encuentroHp string that shows gengar's actual hp and max hp.
	 * 
	 * @param nidohp   string that shows nidorino's actual hp and max hp.
	 * 
	 * @param genvida  int that shows gengar's actual hp.
	 * @param encuentro Salvaje that the character will fight.
	 * @param nidorino Pokemon used by the character.
	 * @param t Board used.
	 * @param f Window used.
	 * @param pokeGratis 
	 */
	
	private static boolean nidottack(boolean turno, String encuentroHp, String nidohp, int genvida, Salvaje encuentro, Team nidorino, Board t, Window f, Boolean pokeGratis)
			throws InterruptedException {
		// TODO Auto-generated method stub

		String[] stats = { "", "Picotazo", "Puya nociva", "Cornada", "Terremoto" };

		t.setText(stats);

		int[][] vidas = { { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 }, { 0, 0 } };

		boolean escoger = true;
		boolean flecha00 = true;
		boolean flecha01 = false;
		boolean flecha10 = false;
		boolean flecha11 = false;

		while (escoger) {

			char movflecha = JuegoPokemon.f.getCurrentKeyPressed();

			int[][] vacio = { { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 },
					{ 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 }, { 0, 0, 0, 0 } };

			BoardSprite s = new BoardSprite("TextBox", 6, 0, 8, 3, "textbox.png");
			BoardSprite txt2 = new BoardSprite("texto", 6, 1, 6, 1, nidorino.ataques[0]);
			txt2.font = new Font("Comic Sans MS", Font.BOLD, 17);
			txt2.text = true;
			BoardSprite txt3 = new BoardSprite("texto", 6, 3, 6, 3, nidorino.ataques[1]);
			txt3.font = new Font("Comic Sans MS", Font.BOLD, 17);
			txt3.text = true;
			BoardSprite txt4 = new BoardSprite("texto", 7, 1, 7, 1, nidorino.ataques[2]);
			txt4.font = new Font("Comic Sans MS", Font.BOLD, 17);
			txt4.text = true;
			BoardSprite txt5 = new BoardSprite("texto", 7, 3, 7, 3, nidorino.ataques[3]);
			txt5.font = new Font("Comic Sans MS", Font.BOLD, 17);
			txt5.text = true;
			BoardSprite flecha1 = new BoardSprite("TextBox", 6, 0, 6, 0, "flecha.png");
			BoardSprite flecha2 = new BoardSprite("TextBox", 6, 2, 6, 2, "flecha.png");
			BoardSprite flecha3 = new BoardSprite("TextBox", 7, 0, 7, 0, "flecha.png");
			BoardSprite flecha4 = new BoardSprite("TextBox", 7, 2, 7, 2, "flecha.png");
			BoardSprite s2 = new BoardSprite("TextBox", 0, 0, 2, 1, "textbox.png");
			BoardSprite genhp = new BoardSprite("texto", 1, 0, 1, 2, encuentroHp);
			genhp.font = new Font("Comic Sans MS", Font.BOLD, 17);
			genhp.text = true;
			BoardSprite s3 = new BoardSprite("TextBox", 3, 2, 5, 3, "textbox.png");
			BoardSprite nidhp = new BoardSprite("texto", 4, 2, 4, 3, nidohp);
			nidhp.font = new Font("Comic Sans MS", Font.BOLD, 17);
			nidhp.text = true;
			ArrayList<BoardSprite> textos = new ArrayList<>();
			textos.add(s);
			textos.add(txt2);
			textos.add(txt3);
			textos.add(txt4);
			textos.add(txt5);
			textos.add(flecha1);
			textos.add(flecha2);
			textos.add(flecha3);
			textos.add(flecha4);
			textos.add(s2);
			textos.add(genhp);
			textos.add(s3);
			textos.add(nidhp);
			if (flecha00) {
				textos.remove(flecha2);
				textos.remove(flecha3);
				textos.remove(flecha4);
				textos.add(flecha1);
				if (movflecha == 'a') {

				} else if (movflecha == 'w') {

				} else if (movflecha == 's') {
					textos.remove(flecha1);
					textos.add(flecha3);
					flecha00 = false;
					flecha10 = true;
				} else if (movflecha == 'd') {
					textos.remove(flecha1);
					textos.add(flecha2);
					flecha00 = false;
					flecha01 = true;
				} else if (movflecha == 'o') {
					encuentro.hp -= ((nidorino.atk * 0.7) - (encuentro.def / 2));
					movimiento('a', vacio, encuentro, t, f, nidorino);
					escoger = false;
				}

			} else if (flecha01) {
				textos.remove(flecha1);
				textos.remove(flecha3);
				textos.remove(flecha4);
				if (movflecha == 'a') {
					textos.remove(flecha2);
					textos.add(flecha1);
					flecha00 = true;
					flecha01 = false;
				} else if (movflecha == 'w') {
				} else if (movflecha == 's') {
					textos.remove(flecha2);
					textos.add(flecha4);
					flecha01 = false;
					flecha11 = true;
				} else if (movflecha == 'd') {
				} else if (movflecha == 'o') {
					encuentro.hp -= ((nidorino.atk * 0.9) - (encuentro.def / 2));
					movimiento('s', vacio, encuentro, t, f, nidorino);
					escoger = false;
				}
			} else if (flecha10) {
				textos.remove(flecha1);
				textos.remove(flecha2);
				textos.remove(flecha4);
				if (movflecha == 'a') {
				} else if (movflecha == 'w') {
					textos.remove(flecha3);
					textos.add(flecha1);
					flecha10 = false;
					flecha00 = true;
				} else if (movflecha == 's') {
				} else if (movflecha == 'd') {
					textos.remove(flecha3);
					textos.add(flecha4);
					flecha10 = false;
					flecha11 = true;
				} else if (movflecha == 'o') {
					encuentro.hp -= ((nidorino.atk * 0.8) - (encuentro.def / 2));
					movimiento('d', vacio, encuentro, t, f, nidorino);
					escoger = false;
				}
			} else if (flecha11) {
				textos.remove(flecha1);
				textos.remove(flecha2);
				textos.remove(flecha3);
				if (movflecha == 'a') {
					textos.remove(flecha4);
					textos.add(flecha3);
					flecha11 = false;
					flecha10 = true;
				} else if (movflecha == 'w') {
					textos.remove(flecha4);
					textos.add(flecha2);
					flecha11 = false;
					flecha01 = true;
				} else if (movflecha == 's') {

				} else if (movflecha == 'd') {

				} else if (movflecha == 'o') {
					encuentro.hp -= (nidorino.atk - (encuentro.def / 2));
					movimiento('w', vacio, encuentro, t, f, nidorino);
					escoger = false;
				}

			}
			
			if (pokeGratis) {
				if (JuegoPokemon.f.getCurrentKeyPressed() == 'u') {
					atrapado=Capturar();
					int[][][] cosa2 = { vacio, mapas.combateAt, vacio };
					char[] tipocosa2 = { 's', 's', 's' };
					t.draw(cosa2, tipocosa2);
					Thread.sleep(1000);
					escoger = false;
				} 
			}
			t.setBoardSprites(textos);
			int[][][] cosa = { vacio, mapas.combate, vacio };
			char[] tipocosa = { 's', 's', 's' };
			if (!atrapado) {
				t.draw(cosa, tipocosa);
			}
		}

		return !turno;

	}

	private static boolean Capturar() {
		// TODO Auto-generated method stub
		
		Random r = new Random();
		
		int n = r.nextInt(4);
		if(n==1) {
			return true;
		}
		return false;
		
	}

	/**
	 * Method used to inform of the move a pokemon used.
	 * 
	 * @param cataq char used to distinguish the different attacks.
	 * 
	 * @param vacio int matrix used by the textboxes.
	 * @param encuentro Salvaje that the character will fight.
	 * @param t Board used.
	 * @param f Window used.
	 * @param nidorino 
	 */
	
	private static void movimiento(char cataq, int[][] vacio, Salvaje encuentro, Board t, Window f, Team nidorino) throws InterruptedException {
		// TODO Auto-generated method stub

		Thread.sleep(200);

		String[] stats = { "", nidorino.name+" usó "+nidorino.ataques[0], nidorino.name+" usó "+nidorino.ataques[1], nidorino.name+" usó "+nidorino.ataques[2],
				nidorino.name+" usó "+nidorino.ataques[3], encuentro.name+" usó "+encuentro.move1.name, encuentro.name+" usó "+encuentro.move2.name,
				encuentro.name+" usó "+encuentro.move3.name, encuentro.name+" usó "+encuentro.move4.name};

		BoardSprite s = new BoardSprite("TextBox", 6, 0, 8, 3, "textbox.png");
		BoardSprite atq1 = new BoardSprite("texto", 7, 1, 7, 2, stats[1]);
		atq1.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq1.text = true;
		BoardSprite atq2 = new BoardSprite("texto", 7, 1, 7, 2, stats[2]);
		atq2.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq2.text = true;
		BoardSprite atq3 = new BoardSprite("texto", 7, 1, 7, 2, stats[3]);
		atq3.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq3.text = true;
		BoardSprite atq4 = new BoardSprite("texto", 7, 1, 7, 2, stats[4]);
		atq4.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq4.text = true;
		BoardSprite atq5 = new BoardSprite("texto", 7, 1, 7, 2, stats[5]);
		atq5.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq5.text = true;
		BoardSprite atq6 = new BoardSprite("texto", 7, 1, 7, 2, stats[6]);
		atq6.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq6.text = true;
		BoardSprite atq7 = new BoardSprite("texto", 7, 1, 7, 2, stats[7]);
		atq7.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq7.text = true;
		BoardSprite atq8 = new BoardSprite("texto", 7, 1, 7, 2, stats[8]);
		atq8.font = new Font("Comic Sans MS", Font.BOLD, 20);
		atq8.text = true;
		ArrayList<BoardSprite> textos = new ArrayList<>();
		textos.add(s);

		int[][][] arrays = { mapas.combate, vacio };
		char[] tipos = { 's', 's' };

		t.setBoardSprites(textos);
		t.setText(stats);
		if (cataq == 'a') {

			textos.add(atq1);
			textos.remove(atq2);
			textos.remove(atq3);
			textos.remove(atq4);
			t.setBoardSprites(textos);
			t.draw(arrays, tipos);

		} else if (cataq == 's') {

			textos.add(atq2);
			textos.remove(atq1);
			textos.remove(atq3);
			textos.remove(atq4);
			t.setBoardSprites(textos);
			t.draw(arrays, tipos);
		} else if (cataq == 'd') {

			textos.add(atq3);
			textos.remove(atq2);
			textos.remove(atq1);
			textos.remove(atq4);
			t.setBoardSprites(textos);
			t.draw(arrays, tipos);
		} else if (cataq == 'w') {

			textos.add(atq4);
			textos.remove(atq2);
			textos.remove(atq3);
			textos.remove(atq1);
			t.setBoardSprites(textos);
			t.draw(arrays, tipos);
		} else if (cataq == 'z') {

			textos.add(atq5);

			t.draw(arrays, tipos);
			Thread.sleep(500);
		} else if (cataq == 'x') {

			textos.add(atq6);

			t.draw(arrays, tipos);
			Thread.sleep(500);
		} else if (cataq == 'c') {

			textos.add(atq7);

			t.draw(arrays, tipos);
			Thread.sleep(500);

		} else if (cataq == 'v') {

			textos.add(atq8);

			t.draw(arrays, tipos);
			Thread.sleep(500);

		}

		Thread.sleep(500);

	}

	/**
	 * Method used when the character's pokemon dies.
	 * @throws IOException 
	 */
	private static void derrota() throws IOException {
		// TODO Auto-generated method stub
		
		int[][] nada = {{0}, {0}};
		JuegoPokemon.t.draw(nada);
		
		String venci = ""+JuegoPokemon.vencidos;
		
		Partida.newRank(JuegoPokemon.nPlayer, venci);
		while(true) {
			JuegoPokemon.t.setImgbackground("dabsVult.gif");
		}
	}
	
}
