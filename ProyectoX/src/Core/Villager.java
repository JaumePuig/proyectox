package Core;

import java.awt.Font;
import java.util.ArrayList;
import java.util.Random;

public class Villager extends Characters{
	
	
	public Villager(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	
	
	/**
	 * This is the method that allows the player to talk to the npc
	 * 
	 * @param npc Npc object with its position in the matrix.
	 * @param mess String with the message that the npc will show.
	 * @param player 2D array where the character moves
	 * @param t Board used.
	 * @param f Window used.
	 * @param b Boolean used when talking to an npc that heals the character's pokemons.
	 * @return 
	 */
	public boolean npcTalk(Characters npc, String mess, int[][] player, Board t, Window f, boolean b) throws InterruptedException{
		
		if (player[x][y+1] == 66) {
			if (f.getCurrentKeyPressed() == 'o') {
				hablar(mess, t, f);
			}
			if(b) {
				JuegoPokemon.nidorino.hp = JuegoPokemon.nidorino.maxhp;
				return true;
			}else {
				return false;
			}
		}
		if (player[x][y-1] == 67) {
			if (f.getCurrentKeyPressed() == 'o') {
				hablar(mess, t, f);
			}
			if(b) {
				JuegoPokemon.nidorino.hp = JuegoPokemon.nidorino.maxhp;
				return true;
			}else {
				return false;
			}
		}
		if (player[x+1][y] == 65) {
			if (f.getCurrentKeyPressed() == 'o') {
				hablar(mess, t, f);
			}
			if(b) {
				JuegoPokemon.nidorino.hp = JuegoPokemon.nidorino.maxhp;
				return true;
			}else {
				return false;
			}
		}
		if (player[x-1][y] == 1) {
			if (f.getCurrentKeyPressed() == 'o') {
				hablar(mess, t, f);
			}
			if(b) {
				JuegoPokemon.nidorino.hp = JuegoPokemon.nidorino.maxhp;
				return true;
			}else {
				return false;
			}
		}
		return false;
	}

	/**
	 * This is the method that the message when an npc talks
	 * 
	 * @param mess2 String with the message that the npc will show.
	 * @param t Board used.
	 * @param f Window used.
	 * @return 
	 */
	private void hablar(String mess2, Board t, Window f) throws InterruptedException {
		// TODO Auto-generated method stub
		String mess = mess2;

		BoardSprite s = new BoardSprite("TextBox", 14, 1, 24, 19, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 19, 3, 17, 15, mess);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 20);
		txt2.text = true;
		ArrayList<BoardSprite> cart = new ArrayList<>();
		cart.add(s);
		cart.add(txt2);
		t.setBoardSprites(cart);

		t.draw(mapas.juego, mapas.tipos);

		while (f.getCurrentKeyPressed() != 'i') {
			Thread.sleep(50);
		}
	}



	/**
	 * This is the method that decides randomly how the npc will move
	 * 
	 * @param npc Npc object with its position in the matrix.
	 */
	public void npcMove(Characters npc) {
		
		Random rand = new Random();

		int num = rand.nextInt(15);

		if (num == 1) {

			int num2 = rand.nextInt(4);

			if (num2 == 0) {

				if (mapas.player[npc.x][npc.y - 1] == 0) {
					mapas.player[npc.x][npc.y] = 0;
					npc.y -= 1;
					mapas.player[npc.x][npc.y] = 88;
				} else {
					mapas.player[npc.x][npc.y] = 88;
				}

			} else if (num2 == 1) {

				if (mapas.player[npc.x][npc.y + 1] == 0) {
					mapas.player[npc.x][npc.y] = 0;
					npc.y += 1;
					mapas.player[npc.x][npc.y] = 89;
				} else {
					mapas.player[npc.x][npc.y] = 89;
				}

			} else if (num2 == 2) {

				if (mapas.player[npc.x - 1][npc.y] == 0) {
					mapas.player[npc.x][npc.y] = 0;
					npc.x -= 1;
					mapas.player[npc.x][npc.y] = 87;
				} else {
					mapas.player[npc.x][npc.y] = 87;
				}

			} else if (num2 == 3) {

				if (mapas.player[npc.x + 1][npc.y] == 0) {
					mapas.player[npc.x][npc.y] = 0;
					npc.x += 1;
					mapas.player[npc.x][npc.y] = 86;
				} else {
					mapas.player[npc.x][npc.y] = 86;
				}

			}

		}
		
	}



	public Boolean giveItem() throws InterruptedException {
		// TODO Auto-generated method stub
		
		if (mapas.player[x][y+1] == 66) {
			if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
				hablar("Toma, poke balls infinitas", JuegoPokemon.t, JuegoPokemon.f);
				return true;
			}
		}
		if (mapas.player[x][y-1] == 67) {
			if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
				hablar("Toma, poke balls infinitas", JuegoPokemon.t, JuegoPokemon.f);
				return true;
			}
		}
		if (mapas.player[x+1][y] == 65) {
			if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
				hablar("Toma, poke balls infinitas", JuegoPokemon.t, JuegoPokemon.f);
				return true;
			}
		}
		if (mapas.player[x-1][y] == 1) {
			if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
				hablar("Toma, poke balls infinitas", JuegoPokemon.t, JuegoPokemon.f);
				return true;
			}
		}
		
		return false;
	}
	
	
}
