package Core;

import java.util.ArrayList;
import java.util.HashMap;

public class Salvaje extends Pokemon{

	static Pokemon gengar = new Pokemon(100, 100, 45, 27, 33, 32, 36, "Gengar", "FrontGengar.gif");
	static Pokemon borrachu = new Pokemon(100, 100, 40, 34, 33, 32, 36, "Borrachu", "Borrachu.png");
	static Pokemon beedrill = new Pokemon(100, 100, 42, 30, 33, 32, 36, "Mega-Beedrill", "FrontMega-Beedrill.gif");
	
	static Pokemon haunter = new Pokemon(100, 100, 33, 25, 33, 32, 36, "Haunter", "FrontHaunter.gif");
	static Pokemon pikachu = new Pokemon(100, 100, 30, 31, 33, 32, 36, "Pikachu", "FrontPikachu.gif");
	static Pokemon nyordrill = new Pokemon(100, 100, 31, 28, 33, 32, 36, "Beedrill", "FrontBeedrill.gif");
	
	Movimientos move1;
	Movimientos move2;
	Movimientos move3;
	Movimientos move4;
	String path2;
	
	static Movimientos Rayo = new Movimientos("Rayo", 0.9);
	static Movimientos BolaSombra = new Movimientos("Bola sombra", 1.0);
	static Movimientos GarraUmbria = new Movimientos("Garra umbria", 0.8);
	static Movimientos PulsoUmbrio = new Movimientos("Pulso umbrío", 0.7);
	static Movimientos AtqRap = new Movimientos("Ataque rápido", 0.45);
	static Movimientos ColaFerrea = new Movimientos("Cola férrea", 0.7);
	static Movimientos PlacElec = new Movimientos("Placaje eléctrico", 1.0);
	static Movimientos Aguijon = new Movimientos("Aguijón letal", 1.0);
	static Movimientos Puya = new Movimientos("Puya nociva", 0.9);
	static Movimientos Pin = new Movimientos("Pin misil", 0.65);
	static Movimientos Persec = new Movimientos("Persecución", 0.4);
	
	public static Pokemon[] lista = {gengar, borrachu, beedrill};
	public static Pokemon[] listaEasy = {haunter, pikachu, nyordrill};
	public static String[] paths = {"BackGengar.gif", "Missingno.png", "BackMBeedrill.gif"};
	public static String[] pathsEasy = {"BackHaunter.gif", "BackPikachu.gif", "BackBeedrill.gif"};
	public static HashMap<String, String> parelles = new HashMap<String, String>();
	public static ArrayList<String> paths2 = new ArrayList<String>();
	public static ArrayList<String> paths2Easy = new ArrayList<String>();
	
	
	public static Movimientos[][] moves = {{BolaSombra, GarraUmbria, PulsoUmbrio, Rayo}, {PlacElec, Rayo, ColaFerrea, AtqRap}, {Aguijon, Puya, Pin, Persec}};

	public Salvaje(int maxhp, int hp, int atk, int def, int satk, int sdef, int spd, String name, String path, int a) {
		super(maxhp, hp, atk, def, satk, sdef, spd, name, path);
		move1 = moves[a][0];
		move2 = moves[a][1];
		move3 = moves[a][2];
		move4 = moves[a][3];
		
		parelles.put("Gengar", "BackGengar.gif");
		parelles.put("Borrachu", "Missingno.png");
		parelles.put("Mega-Beedrill", "BackMBeedrill.gif");
		parelles.put("Haunter", "BackHaunter.gif");
		parelles.put("Pikachu", "BackPikachu.gif");
		parelles.put("Beedrill", "BackBeedrill.gif");
		
		path2 = parelles.get(name);
		
	}

	@Override
	public String toString() {
		return "Salvaje [name=" + name + ", path=" + path + "]";
	}
	
	
	
}
