package Core;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

public class Rutas extends mapas {

	/**
	 * One of the main matrixes for the route, where the background objects are.
	 */
	static int[][] ruta1A = { { 6, 6, 6, 6, 8, 7, 9, 6, 6, 6, 6 }, { 6, 14, 14, 14, 8, 7, 9, 5, 5, 5, 6 },
			{ 6, 14, 14, 14, 8, 7, 9, 5, 5, 5, 6 }, { 6, 14, 14, 14, 8, 7, 9, 5, 5, 5, 6 },
			{ 6, 14, 14, 14, 8, 7, 7, 11, 11, 13, 6 }, { 6, 14, 14, 14, 8, 7, 7, 7, 7, 9, 6 },
			{ 6, 14, 14, 14, 10, 12, 12, 7, 7, 9, 6 }, { 6, 6, 6, 6, 6, 6, 6, 8, 7, 9, 6 },
			{ 6, 5, 5, 5, 5, 5, 5, 8, 7, 9, 6 }, { 6, 5, 5, 5, 5, 5, 5, 14, 14, 14, 6 },
			{ 6, 16, 11, 11, 11, 14, 14, 14, 14, 14, 6 }, { 6, 8, 7, 7, 7, 14, 14, 14, 14, 14, 6 },
			{ 6, 8, 7, 7, 12, 14, 14, 14, 14, 14, 6 }, { 6, 8, 7, 9, 5, 14, 14, 14, 14, 14, 6 },
			{ 6, 8, 7, 9, 5, 5, 5, 5, 5, 5, 6 }, { 6, 8, 7, 9, 5, 5, 5, 5, 5, 5, 6 },
			{ 11, 7, 7, 9, 5, 14, 14, 14, 14, 5, 6 }, { 7, 7, 7, 9, 5, 14, 14, 14, 14, 5, 6 },
			{ 12, 12, 12, 18, 5, 14, 14, 14, 14, 5, 6 }, { 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 } };

	/**
	 * One of the main matrixes for the route, where the main character is.
	 */
	static int[][] ruta1B = { { 6, 6, 6, 6, 0, 0, 0, 6, 6, 6, 6 }, { 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6 },
			{ 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6 }, { 6, 6, 6, 6, 6, 6, 6, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 0, 0, 0, 17, 0, 0, 0, 0, 0, 6 },
			{ 6, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6 }, { 6, 0, 0, 0, 0, 6, 6, 6, 6, 6, 6 },
			{ 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 19, 0, 0, 0, 0, 0, 0, 0, 0, 15, 6 },
			{ 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6 }, { 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6 } };

	/**
	 * Array combining the two route main matrixes.
	 */
	static int[][][] ruta1 = { ruta1A, ruta1B };

	/**
	 * Array specifying the route main matrixes types.
	 */
	static char[] tipos1 = { 's', 's' };

	/**
	 * Hollow array used for textboxes.
	 */
	static int[][] vacio = { { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 },
			{ 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 } };

	/**
	 * Array combining the two route main matrixes and the hollow one.
	 */
	static int[][][] ruta1Alt = { ruta1A, ruta1B, vacio };
	
	/**
	 * Array specifying the route main matrixes types and the hollow one.
	 */
	static char[] tipos1Alt = { 's', 's', 's' };
	/**
	 * Array with all the sprite paths.
	 */
	static String[] ruteTiles = { "", "RedGif.gif", "RedGifBack.gif", "RedGifLat.gif", "RedGifLat2.gif", "Hierba1.png",
			"Tree.png", "centerSand.png", "leftSand.png", "rightSand.png", "Sandbl.png", "upperSand.png", "Sandbot.png",
			"SandEsqUR.png", "tallGrass.png", "pokeBall.png", "SandEsqUL.png", "cartel.png", "Sandbr.png", "roca.png" };
	/*
	 * 0:null 10:Sandbl 1:RedGif 11:upperSand 2:RedGifBack 12:Sandbot 3:RedGifLat
	 * 13:SandEsqUR 4:RedGifLat2 14:tallGrass 5:Hierba1 15:pokeball 6:Tree
	 * 16:SandEsqUL 7:centerSand 17:cartel 8:leftSand 18:Sandbr 9:rightSand 19:roca
	 */
	/**
	 * Item object found in the route.
	 */
	static Item poke = new Item("PokeBall", "Sirve para atrapar pokemones");
	/**
	 * Method used to enter the route.
	 * @param red Red object that enters the house.
	 * @param team 
	 * @param pokeGratis 
	 * @param nPlayer 
	 * @param lider 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void entrarRuta(Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer, int lider) throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub

		JuegoPokemon.t.setSprites(ruteTiles);

		JuegoPokemon.t.setColorbackground(0x51CA75);

		red.x -= 5;
		red.y = 1;

		JuegoPokemon.t.draw(ruta1, tipos1);

		ruta1B[red.y][red.x] = 1;

		JuegoPokemon.f.playMusic("route1.wav");
		
		Thread.sleep(120);
		
		route(red, team, pokeGratis, nPlayer, lider);

		ruta1B[red.y][red.x] = 0;

	}
	public static void route(Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer, int lider) throws InterruptedException, IOException, ClassNotFoundException {
		
		boolean salir = false;
		boolean fight = false;
		boolean get = false;
		boolean music = false;
		ArrayList<BoardSprite> hollow = new ArrayList<>();
		
		ruta1B[red.y][red.x] = 1;
		
		while (!salir) {

			JuegoPokemon.t.setSprites(ruteTiles);
			JuegoPokemon.t.setColorbackground(0x51CA75);
			JuegoPokemon.t.setBoardSprites(hollow);
			JuegoPokemon.t.setActimgbackground(false);
			fight = false;
			
			JuegoPokemon.t.draw(ruta1, tipos1);

			char c = JuegoPokemon.f.getCurrentKeyPressed();

			fight = red.redMoveRuta(c, fight, ruta1B);
			
			if (music) {
				JuegoPokemon.f.playMusic("route1.wav");
				music = false;
			}
			if(JuegoPokemon.f.getCurrentKeyPressed()=='t') {
				ruta1B[red.y][red.x] = 0;
				Partida.cargar();
				Thread.sleep(150);
			}
			if(JuegoPokemon.f.getCurrentKeyPressed()=='p') {
				Menu.menu(2, red, team, pokeGratis, nPlayer, lider);
				lider = JuegoPokemon.lidel;
			}
			
			if (ruta1A[red.y][red.x] == 14) {

				if (fight) {
					if (JuegoPokemon.encuentro() == 1) {
						JuegoPokemon.f.playMusic("wildBattle.wav");
						JuegoPokemon.allyHp = Combate.combatInit(JuegoPokemon.t, JuegoPokemon.f, JuegoPokemon.allyHp,
								JuegoPokemon.nidorino, JuegoPokemon.alive, 2, pokeGratis, team, lider);
						music = true;
						
						JuegoPokemon.t.draw(ruta1, tipos1);

					}
				}

			}

			if (red.y == 14 && red.x == 4 && ruta1B[14][4] == 2) {
				if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
					cartelRuta1();
				}
			}

			if (red.y == 18 && red.x == 9 && ruta1B[18][9] == 2 && !get) {
				if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
					get=obtenerItem(poke);
					ruta1B[17][9] = 0;
				}
			}
			if (red.y == 17 && red.x == 8 && ruta1B[17][8] == 4 && !get) {
				if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
					get=obtenerItem(poke);
					ruta1B[17][9] = 0;
				}
			}
			if (red.y == 16 && red.x == 9 && ruta1B[16][9] == 1 && !get) {
				if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {
					get=obtenerItem(poke);
					ruta1B[17][9] = 0;
				}

			}

			if ((red.y == 0 && red.x == 4) || (red.y == 0 && red.x == 5) || (red.y == 0 && red.x == 6)) {
				salir = true;
			}

			Thread.sleep(120);
		}
	}
	/**
	 * Method used to obtain an item.
	 * @param obj Item to be obtained.
	 */
	private static boolean obtenerItem(Item obj) throws InterruptedException {
		// TODO Auto-generated method stub

		Inventario.newItem(obj, 1);

		String nombre = obj.name;
		String mess = "Has obtenido 1 " + nombre;

		BoardSprite s = new BoardSprite("TextBox", 2, 0, 5, 7, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 3, 2, 4, 4, mess);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 15);
		txt2.text = true;

		ArrayList<BoardSprite> cart = new ArrayList<>();
		cart.add(s);
		cart.add(txt2);

		JuegoPokemon.t.setBoardSprites(cart);
		JuegoPokemon.t.draw(ruta1Alt, tipos1Alt);
		while (JuegoPokemon.f.getCurrentKeyPressed() != 'i') {
			Thread.sleep(50);
		}
		
		return true;
		
	}
	/**
	 * Method that shows a message.
	 */
	public static void cartelRuta1() throws InterruptedException {

		String mess = "Recuerda buscar bien en las rutas,";
		String mess2 = "para encontrar objetos escondidos.";

		BoardSprite s = new BoardSprite("TextBox", 2, 0, 5, 7, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 3, 1, 4, 3, mess);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 15);
		txt2.text = true;
		BoardSprite txt3 = new BoardSprite("texto", 4, 1, 5, 3, mess2);
		txt3.font = new Font("Comic Sans MS", Font.BOLD, 15);
		txt3.text = true;
		ArrayList<BoardSprite> cart = new ArrayList<>();
		cart.add(s);
		cart.add(txt2);
		cart.add(txt3);
		JuegoPokemon.t.setBoardSprites(cart);

		JuegoPokemon.t.draw(ruta1Alt, tipos1Alt);

		while (JuegoPokemon.f.getCurrentKeyPressed() != 'i') {
			Thread.sleep(50);
		}
	}
}
