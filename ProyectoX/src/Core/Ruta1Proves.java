package Core;

import java.awt.Font;
import java.util.ArrayList;

public class Ruta1Proves {

	//clase de ruta de prueba
	//11x20
	static int[][] ruta1A = {{6,6,6,6,8,7,9,6,6,6,6},
			{6,14,14,14,8,7,9,5,5,5,6},
			{6,14,14,14,8,7,9,5,5,5,6},
			{6,14,14,14,8,7,9,5,5,5,6},
			{6,14,14,14,8,7,7,11,11,13,6},
			{6,14,14,14,8,7,7,7,7,9,6},
			{6,14,14,14,10,12,12,7,7,9,6},
			{6,6,6,6,6,6,6,8,7,9,6},
			{6,5,5,5,5,5,5,8,7,9,6},
			{6,5,5,5,5,5,5,14,14,14,6},
			{6,16,11,11,11,14,14,14,14,14,6},
			{6,8,7,7,7,14,14,14,14,14,6},
			{6,8,7,7,12,14,14,14,14,14,6},
			{6,8,7,9,5,14,14,14,14,14,6},
			{6,8,7,9,5,5,5,5,5,5,6},
			{6,8,7,9,5,5,5,5,5,5,6},
			{11,7,7,9,5,14,14,14,14,5,6},
			{7,7,7,9,5,14,14,14,14,5,6},
			{12,12,12,18,5,14,14,14,14,5,6},
			{6,6,6,6,6,6,6,6,6,6,6}};
	
	static int[][] ruta1B = {{6,6,6,6,0,0,0,6,6,6,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,6,6,6,6,6,6,0,0,0,6},
			{6,6,6,6,6,6,6,0,0,0,6},
			{6,6,6,6,6,6,6,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,0,0,0,0,0,0,6},
			{6,0,0,0,17,0,0,0,0,0,6},
			{6,0,0,0,0,6,6,6,6,6,6},
			{6,0,0,0,0,6,6,6,6,6,6},
			{0,0,0,0,0,0,0,0,0,0,6},
			{0,0,0,0,0,0,0,0,0,15,6},
			{0,0,0,0,0,0,0,0,0,0,6},
			{6,6,6,6,6,6,6,6,6,6,6}};
	
	static int[][][] ruta1 = {ruta1A, ruta1B};
	
	static char[] tipos = {'s','s'};
	
	static int[][] vacio = { { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, 
			{ 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }, 
			{ 0, 0, 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0, 0, 0 }};
	
	static int[][][] rutasAlt = {ruta1A, ruta1B, vacio};
	static char[] tiposAlt = {'s','s','s'};
	
	static String[] ruteTiles = { "","RedGif.gif", "RedGifBack.gif", "RedGifLat.gif", "RedGifLat2.gif", "Hierba1.png",
			"Tree.png", "centerSand.png", "leftSand.png", "rightSand.png", "Sandbl.png", "upperSand.png", "Sandbot.png",
			"SandEsqUR.png", "tallGrass.png", "pokeBall.png", "SandEsqUL.png", "cartel.png", "Sandbr.png"};
	/*
	 * 0:null		10:Sandbl
	 * 1:RedGif		11:upperSand
	 * 2:RedGifBack	12:Sandbot
	 * 3:RedGifLat	13:SandEsqUR
	 * 4:RedGifLat2	14:tallGrass
	 * 5:Hierba1	15:pokeball
	 * 6:Tree		16:SandEsqUL
	 * 7:centerSand	17:cartel
	 * 8:leftSand	18:Sandbr
	 * 9:rightSand	19:
	 */
	
	static Board t = new Board();
	
	static Window f = new Window(t);
	
	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		t.setColorbackground(0x51CA75);
		t.setActborder(false);
		t.setSprites(ruteTiles);
		f.setActLabels(false);
		t.draw(ruta1, tipos);
	/*
		while(true) {
			cartelRuta1();
		}
	*/
	}
	
	public static void cartelRuta1() throws InterruptedException {

		String mess = "Recuerda buscar bien en las rutas,";
		String mess2 = "para encontrar objetos escondidos.";

		BoardSprite s = new BoardSprite("TextBox", 2, 0, 5, 7, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 3, 1, 4, 3, mess);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 20);
		txt2.text = true;
		BoardSprite txt3 = new BoardSprite("texto", 4, 1, 5, 3, mess2);
		txt3.font = new Font("Comic Sans MS", Font.BOLD, 20);
		txt3.text = true;
		ArrayList<BoardSprite> cart = new ArrayList<>();
		cart.add(s);
		cart.add(txt2);
		cart.add(txt3);
		t.setBoardSprites(cart);

		t.draw(rutasAlt, tiposAlt);

		while (f.getCurrentKeyPressed() != 'i') {
			Thread.sleep(50);
		}
	}
	

}
