package Core;

import java.io.Serializable;

/**
 * Class with the information about every pokemon object.
 * 
 * @author Jaume Puig
 * @version 0.6
 *
 */

public class Pokemon implements Serializable {

	int maxhp;
	int hp;
	int atk;
	int def;
	int satk;
	int sdef;
	int spd;
	String name;
	String path;
	
	public Pokemon(int maxhp, int hp, int atk, int def, int satk, int sdef, int spd, String name, String path) {
		super();
		this.maxhp = maxhp;
		this.hp = hp;
		this.atk = atk;
		this.def = def;
		this.satk = satk;
		this.sdef = sdef;
		this.spd = spd;
		this.name = name;
		this.path = path;
	}
	
	
	
	
	
}
