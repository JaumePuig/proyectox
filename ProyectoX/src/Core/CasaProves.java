package Core;

public class CasaProves {

	//clase de casa de pruebas
	static int[][] interior = {{32,32,32,32,32,32,32,32,32,32,32,32,32},
			{32,3,4,4,4,4,4,4,4,4,4,5,32},
			{32,6,7,7,7,7,7,7,7,7,7,8,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,13,0,0,14,1,15,16,24,17,2,32},
			{32,2,13,0,0,14,1,18,19,19,20,2,32},
			{32,2,1,2,1,2,1,21,25,22,23,2,32},
			{32,2,1,2,1,2,26,27,1,2,1,2,32},
			{32,32,32,32,32,32,32,32,32,32,32,32,32}};
	
	static int[][] interior2 = {{32,32,32,32,32,32,32,32,32,32,32,32,32},
			{32,3,4,4,4,4,4,4,4,4,4,5,32},
			{32,6,7,7,7,7,7,7,7,7,7,8,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,9,10,0,0,0,0,0,0,0,32},
			{32,0,0,11,12,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,32,32,32,32,32,32,32,32,32,32,32,32}};
	
	static int[][] interior3 = {{32,32,32,32,32,32,32,32,32,32,32,32,32},
			{32,2,4,4,4,4,4,4,4,4,4,5,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,32,32,32,32,32,32,32,32,32,32,32,32}};
	
	
	static int[][][] cosas = {interior3, interior, interior2};
	
	static char[] charcosas = {'s', 's', 's'};
	
	
	public static String[] tileInterior = {"", "madera10.png", "madera11.png", "pared00.png", "pared01.png", "pared02.png", "pared10.png", "pared11.png", "pared12.png",
			"mesa00.png", "mesa01.png", "mesa10.png", "mesa11.png", "taburete.png", "taburete2.png", "alfombra00.png", "alfombra01.png", "alfombra02.png",
			"alfombra10.png", "alfombracentro2.png", "alfombra12.png", "alfombra202.png", "alfombra212.png", "alfombra222.png", "alfombra002.png", "alfombra211.png",
			"entrada1.png", "entrada2.png", "RedGif.gif", "RedGifBack.gif", "RedGifLat.gif", "RedGifLat2.gif", ""};
	
	/*
	 * 0:nada		10:mesa01			20:alfombra12	30:RedGifLat
	 * 1:madera10	11:mesa10			21:alfombra202	31:RedGifLat2
	 * 2:madera11	12:mesa11			22:alfombra212	32:nada2
	 * 3:pared00	13:taburete			23:alfombra222
	 * 4:pared01	14:taburete2		24:alfombra002
	 * 5:pared02	15:alfombra00		25:alfombra211
	 * 6:pared10	16:alfombra01		26:entrada1
	 * 7:pared11	17:alfombra02		27:entrada2
	 * 8:pared12	18:alfombra10		28:RedGif
	 * 9:mesa00		19:alfombracentro2	29:RedGifBack
	 */
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Board t = new Board();
		
		Window f = new Window(t);
		
		t.setActborder(false);
		t.setColorbackground(0x000000);
		
		t.setSprites(tileInterior);
		f.setActLabels(false);
		
		t.draw(cosas, charcosas);
	}

}
