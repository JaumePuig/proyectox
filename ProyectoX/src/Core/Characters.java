package Core;

import java.io.Serializable;
import java.util.Random;

/**
 * Class with the parameters marking a position in a matrix.
 * 
 * @author Jaume Puig
 * @version 0.6
 *
 */

public abstract class Characters implements Serializable {

	int x;
	int y;
	
	public Characters(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
}