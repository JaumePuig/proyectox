package Core;

import java.awt.Font;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.GregorianCalendar;

public class Partida {
	
	
	public static void guardar(int[] arr, Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer) throws IOException, ClassNotFoundException {
		
		File f = new File("Resources/Partida");
		FileOutputStream fos = new FileOutputStream(f);
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		
		oos.writeObject(arr);
		oos.writeObject(red);
		oos.writeObject(team);
		oos.writeObject(pokeGratis);
		oos.writeObject(nPlayer);
		
		oos.flush();
		oos.close();
		
		System.out.println("Guardar");
		
		
	}
	
	public static void newRank(String nPlayer, String vencidos) throws IOException {
		
		System.out.println("adfbadpfi");
		
		File f = new File("Resources/Ranking");
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);
		
		ArrayList<Rank> ranking = new ArrayList<Rank>();
		
		while(br.ready()) {
			String s = br.readLine();
			
			String[] arrS = s.split(",");
			
			int venc = Integer.parseInt(arrS[1]);
			String data = arrS[2];
			
			Rank nuevo = new Rank(arrS[0], venc, data);
			
			ranking.add(nuevo);
			
		}
		
		br.close();
		
		Rank jugador = new Rank(nPlayer, vencidos);
		
		ranking.add(jugador);
		
		Collections.sort(ranking);
		
		if(ranking.size()>10){
			
			for (int i = ranking.size()-1; i > 10; i--) {
				ranking.remove(i);
			}
			
		}
		
		FileWriter out = new FileWriter(f);
		BufferedWriter bw = new BufferedWriter(out);
		
		for (int i = 0; i < ranking.size(); i++) {
			
			String pos = ranking.get(i).nPLayer+","+ranking.get(i).vencidos+","+ranking.get(i).calen;
			
			bw.write(pos);
			bw.newLine();
			
		}
		
		bw.flush();
		bw.close();
		
	}
	
	public static void ranking() throws IOException, InterruptedException {
		
		File f = new File("Resources/Ranking");
		FileReader in = new FileReader(f);
		BufferedReader br = new BufferedReader(in);
		
		ArrayList<String> ranking = new ArrayList<String>();
		
		while(br.ready()) {
			String s = br.readLine();
			
			ranking.add(s);
			
		}
		
		
		int[][] mat = { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }};
		
		BoardSprite txt = new BoardSprite("texto", 1, 2, 1, 2, "RANKING:");
		txt.font = new Font("Comic Sans MS", Font.BOLD, 14);
		txt.text = true;
		
		ArrayList<BoardSprite> ranks = new ArrayList<>();
		ranks.add(txt);
		
		for (int i = 0; i < ranking.size(); i++) {
			
			BoardSprite txt2 = new BoardSprite("texto", 3+i, 2, 1+i, 2, ranking.get(i));
			txt2.font = new Font("Comic Sans MS", Font.BOLD, 12);
			txt2.text = true;
			ranks.add(txt2);
		}
		
		JuegoPokemon.t.setBoardSprites(ranks);
		JuegoPokemon.t.setColorbackground(0xFFFFFF);
		while(JuegoPokemon.f.getCurrentKeyPressed()!='i') {
			
			JuegoPokemon.t.draw(mat, 's');
			
			Thread.sleep(150);
			
		}
		
	}
	
	public static void cargar() throws IOException, ClassNotFoundException, InterruptedException {
		
		File f2 = new File("Resources/Partida");
		FileInputStream fis = new FileInputStream(f2);
		ObjectInputStream ois = new ObjectInputStream(fis);
		
		int map = 1;
		int lider = 0;
		int[] arr = new int[2];
		Red red = new Red(7, 10);
		ArrayList<Team> team = new ArrayList<Team>();
		Boolean pokeGratis = false;
		String nPlayer = "";
		
		for (int i = 0; i < 4; i++) {
			
			Object o = ois.readObject();
			
			if(o instanceof int[]) {
				
				arr = (int[]) o;
				
			} else if (o instanceof Red) {
				
				red = (Red) o;
				
			} else if(o instanceof Boolean){
				pokeGratis = (Boolean) o;
			} else if(o instanceof String) {
				nPlayer = (String) o;
			} else {
				
				team = (ArrayList<Team>) o;
				
			}
			
		}
		
		map = arr[0];
		lider = arr[1];
		
//		while(f2.canRead()) {
//			
//			Object o = ois.readObject();
//			
//			if(o instanceof Integer) {
//				
//				map = (Integer) o;
//				
//			} else if (o instanceof Red) {
//				
//				red = (Red) o;
//				
//			} else {
//				
//				team = (ArrayList<Team>) o;
//				
//			}
//			
//		}
		
		if(map==1) {
			JuegoPokemon.pueblo(red, team, pokeGratis, nPlayer, lider);
		}else if(map==2) {
			Rutas.route(red, team, pokeGratis, nPlayer, lider);
		}
		
	}
	
}
