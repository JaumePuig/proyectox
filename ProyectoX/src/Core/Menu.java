package Core;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

public class Menu {
	
	public static void menu(int mapa, Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer, int lider) throws InterruptedException, IOException, ClassNotFoundException {

		String equipo = "Pokemon";
		String moch = "Mochila";
		String guardar = "Guardar partida";
		String ranking = "Ranking";
		String noPodes = "Las palabras del Prof. Oak resuenan en tu cabeza:";
		String noPodes2 = "Deja de meterte en casas ajenas y completa la pokedex";

		int[][] vacio2 = { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };

		int[][] map1;
		int[][] map2;
		int[][][] map;
		
		if (mapa == 1) {
			map1 = mapas.fons;
			map2 = mapas.player;
			int[][][] maptot = { vacio2, map1, map2, vacio2 };
			map = maptot;
		} else if(mapa == 3) {
			map1 = mapas.fondoCasa;
			map2 = mapas.casa;
			int[][][] maptot = { vacio2, map1, map2, vacio2 };
			map = maptot;
		} else {
			map1 = Rutas.ruta1A;
			map2 = Rutas.ruta1B;
			int[][][] maptot = { vacio2, map1, map2, vacio2 };
			map = maptot;
		}
		char[] lmap = { 's', 's', 's', 's' };
		BoardSprite s = new BoardSprite("TextBox", 0, 3, 5, 5, "textbox.png");
		BoardSprite txt1 = new BoardSprite("texto", 1, 4, 1, 4, equipo);
		txt1.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt1.text = true;
		BoardSprite txt2 = new BoardSprite("texto", 2, 4, 2, 4, moch);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt2.text = true;
		BoardSprite txt3 = new BoardSprite("texto", 3, 4, 3, 4, guardar);
		txt3.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt3.text = true;
		BoardSprite txt6 = new BoardSprite("texto", 4, 4, 4, 4, ranking);
		txt6.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt6.text = true;
		BoardSprite flecha1 = new BoardSprite("TextBox", 1, 3, 1, 3, "flecha.png");
		BoardSprite flecha2 = new BoardSprite("TextBox", 2, 3, 2, 3, "flecha.png");
		BoardSprite flecha3 = new BoardSprite("TextBox", 3, 3, 3, 3, "flecha.png");
		BoardSprite flecha4 = new BoardSprite("TextBox", 4, 3, 4, 3, "flecha.png");
		BoardSprite s2 = new BoardSprite("TextBox", 8, 0, 11, 5, "textbox.png");
		BoardSprite txt4 = new BoardSprite("texto", 9, 1, 9, 1, noPodes);
		txt4.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt4.text = true;
		BoardSprite txt5 = new BoardSprite("texto", 10, 1, 10, 1, noPodes2);
		txt5.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt5.text = true;
		ArrayList<BoardSprite> hps = new ArrayList<>();
		hps.add(s);
		hps.add(txt1);
		hps.add(txt2);
		hps.add(txt3);
		hps.add(txt6);
		hps.add(flecha1);
		hps.add(flecha2);
		hps.add(flecha3);
		hps.add(flecha4);
		

		boolean flecha00 = true;
		boolean flecha01 = false;
		boolean flecha02 = false;
		boolean flecha03 = false;

		JuegoPokemon.t.setBoardSprites(hps);
		JuegoPokemon.t.draw(map, lmap);

		while (JuegoPokemon.f.getCurrentKeyPressed() != 'i') {

			char movfl = JuegoPokemon.f.getCurrentKeyPressed();

			JuegoPokemon.t.setBoardSprites(hps);
			JuegoPokemon.t.draw(map, lmap);
			if (flecha00) {
				hps.remove(flecha2);
				hps.remove(flecha3);
				hps.remove(flecha4);
				if (movfl == 'w') {

				} else if (movfl == 's') {
					hps.remove(flecha1);
					hps.add(flecha2);
					flecha00 = false;
					flecha01 = true;
				} else if (movfl == 'o') {
					menuPokes(map, lmap, lider);
				}

			} else if (flecha01) {
				hps.remove(flecha1);
				hps.remove(flecha3);
				hps.remove(flecha4);
				if (movfl == 'w') {
					hps.remove(flecha2);
					hps.add(flecha1);
					flecha00 = true;
					flecha01 = false;
				} else if (movfl == 's') {
					hps.remove(flecha2);
					hps.add(flecha3);
					flecha02 = true;
					flecha01 = false;
				} else if (movfl == 'o') {
					mochila(pokeGratis, map, lmap);
				}
			} else if (flecha02) {
				hps.remove(flecha1);
				hps.remove(flecha2);
				hps.remove(flecha4);
				if (movfl == 'w') {
					hps.remove(flecha3);
					hps.add(flecha2);
					flecha01 = true;
					flecha02 = false;
				} else if (movfl == 's') {
					
					hps.remove(flecha3);
					hps.add(flecha4);
					flecha03 = true;
					flecha02 = false;

				} else if (movfl == 'o') {
					if (mapa!=3) {
						int[] arr = {mapa, lider};
						Partida.guardar(arr, red, JuegoPokemon.team, pokeGratis, nPlayer);
					}else {
						hps.add(s2);
						hps.add(txt4);
						hps.add(txt5);
						while (JuegoPokemon.f.getCurrentKeyPressed() != 'i') {
							
						}
						hps.remove(s2);
						hps.remove(txt4);
						hps.remove(txt5);
					}
				}
			} else if (flecha03) {
				hps.remove(flecha1);
				hps.remove(flecha2);
				hps.remove(flecha3);
				if (movfl == 'w') {
					hps.remove(flecha4);
					hps.add(flecha3);
					flecha02 = true;
					flecha03 = false;
				} else if (movfl == 's') {

				} else if (movfl == 'o') {
					
					Partida.ranking();
					
				}
			}

			Thread.sleep(100);
		}
		
		hps.clear();

	}

	private static void mochila(Boolean pokeGratis, int[][][] map, char[] lmap) throws InterruptedException {
		// TODO Auto-generated method stub
		
		BoardSprite s = new BoardSprite("TextBox", 0, 3, 5, 5, "textbox.png");
		
		BoardSprite flecha1 = new BoardSprite("TextBox", 1, 3, 1, 3, "flecha.png");
		ArrayList<BoardSprite> hps2 = new ArrayList<>();
		hps2.add(s);
		hps2.add(flecha1);
		for (int i = 0; i < JuegoPokemon.team.size(); i++) {
			BoardSprite txt = new BoardSprite("texto", i+1, 4, i+1, 4, JuegoPokemon.team.get(i).name);
			txt.font = new Font("Comic Sans MS", Font.BOLD, 12);
			txt.text = true;
			hps2.add(txt);
		}
		if (pokeGratis) {
			BoardSprite txt = new BoardSprite("texto", 1, 4, 1, 4, "PokeBalls x99999999999");
			txt.font = new Font("Comic Sans MS", Font.BOLD, 12);
			txt.text = true;
			hps2.add(txt);
		}else {
			BoardSprite txt = new BoardSprite("texto", 1, 4, 1, 4, "Vacía");
			txt.font = new Font("Comic Sans MS", Font.BOLD, 12);
			txt.text = true;
			hps2.add(txt);
		}
		
		JuegoPokemon.t.setBoardSprites(hps2);
		JuegoPokemon.t.draw(map, lmap);
		
		while(JuegoPokemon.f.getCurrentKeyPressed()!='i') {
			
			Thread.sleep(200);
			
		}
		
	}

	private static void menuPokes(int[][][] map, char[] lmap, int lider) throws InterruptedException {
		// TODO Auto-generated method stub
		
		Thread.sleep(100);
		
//		Team nidorina = new Team(100, 100, 47, 29, 33, 32, 36, 1, 0, "Nidorina", "BackNidorino.gif");
//		Team Missingno = new Team(100, 100, 47, 29, 33, 32, 36, 1, 0, "Missingno", "BackNidorino.gif");
		
//		team.add(nidorina);
//		team.add(Missingno);
		
		
		int[][] vacio2 = { { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 },
				{ 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 }};

		BoardSprite s = new BoardSprite("TextBox", 0, 3, 5, 5, "textbox.png");
		
		BoardSprite flecha1 = new BoardSprite("TextBox", 1, 3, 1, 3, "flecha.png");
		BoardSprite flecha2 = new BoardSprite("TextBox", 2, 3, 2, 3, "flecha.png");
		BoardSprite flecha3 = new BoardSprite("TextBox", 3, 3, 3, 3, "flecha.png");
		ArrayList<BoardSprite> hps2 = new ArrayList<>();
		hps2.add(s);
		hps2.add(flecha1);
		hps2.add(flecha2);
		hps2.add(flecha3);
		for (int i = 0; i < JuegoPokemon.team.size(); i++) {
			BoardSprite txt = new BoardSprite("texto", i+1, 4, i+1, 4, JuegoPokemon.team.get(i).name);
			txt.font = new Font("Comic Sans MS", Font.BOLD, 12);
			txt.text = true;
			hps2.add(txt);
		}
		
		boolean flecha00 = true;
		boolean flecha01 = false;
		boolean flecha02 = false;
		boolean chlider = false;
		
		JuegoPokemon.t.setBoardSprites(hps2);
		JuegoPokemon.t.draw(map, lmap);
		
		while(JuegoPokemon.f.getCurrentKeyPressed()!='i') {
			
			char movfl = JuegoPokemon.f.getCurrentKeyPressed();

			JuegoPokemon.t.draw(map, lmap);
			
			JuegoPokemon.t.setBoardSprites(hps2);
			
			if (flecha00) {
				hps2.remove(flecha2);
				hps2.remove(flecha3);
				if (movfl == 'w') {

				} else if (movfl == 's') {
					hps2.remove(flecha1);
					hps2.add(flecha2);
					flecha00 = false;
					flecha01 = true;
				} else if (movfl == 'o') {
					chlider = verPoke(JuegoPokemon.team.get(0), map, lmap);
					if(chlider) {
						JuegoPokemon.lidel=0;
					}
				}

			} else if (flecha01) {
				hps2.remove(flecha1);
				hps2.remove(flecha3);
				if (movfl == 'w') {
					hps2.remove(flecha2);
					hps2.add(flecha1);
					flecha00 = true;
					flecha01 = false;
				} else if (movfl == 's') {
					hps2.remove(flecha2);
					hps2.add(flecha3);
					flecha02 = true;
					flecha01 = false;
				} else if (movfl == 'o') {
					chlider = verPoke(JuegoPokemon.team.get(1), map, lmap);
					if(chlider) {
						JuegoPokemon.lidel=1;
					}
				}
			} else if (flecha02) {
				hps2.remove(flecha1);
				hps2.remove(flecha2);
				if (movfl == 'w') {
					hps2.remove(flecha3);
					hps2.add(flecha2);
					flecha01 = true;
					flecha02 = false;
				} else if (movfl == 's') {

				} else if (movfl == 'o') {
					chlider = verPoke(JuegoPokemon.team.get(2), map, lmap);
					if(chlider) {
						JuegoPokemon.lidel=2;
					}
				}
			}

			Thread.sleep(100);
			
		}
		
		
	}

	private static boolean verPoke(Team team, int[][][] map, char[] lmap) throws InterruptedException {
		// TODO Auto-generated method stub
		
		
		String vida = "HP "+team.hp+"/"+team.maxhp;
		String lvl = "Lvl "+team.lvl;
		String ataq = "Atk "+team.atk;
		String def = "Def "+team.def;
		String satk = "Sp.Atk "+team.satk;
		String sdef = "Sp.Def "+team.sdef;
		String spd = "Spd "+team.spd;
		boolean retorno = false;
		
		BoardSprite s = new BoardSprite("TextBox", 0, 0, 3, 2, "textbox.png");
		BoardSprite poke = new BoardSprite("TextBox", 0, 0, 3, 2, team.path2);
		BoardSprite s2 = new BoardSprite("TextBox", 0, 3, 3, 5, "textbox.png");
		BoardSprite txt = new BoardSprite("texto", 1, 4, 1, 4, team.name);
		txt.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt.text = true;
		BoardSprite s3 = new BoardSprite("TextBox", 4, 0, 8, 5, "textbox.png");
		BoardSprite txt2 = new BoardSprite("texto", 5, 1, 5, 1, vida);
		txt2.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt2.text = true;
		BoardSprite txt3 = new BoardSprite("texto", 6, 1, 6, 1, ataq);
		txt3.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt3.text = true;
		BoardSprite txt4 = new BoardSprite("texto", 5, 3, 5, 3, lvl);
		txt4.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt4.text = true;
		BoardSprite txt5 = new BoardSprite("texto", 6, 3, 6, 3, def);
		txt5.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt5.text = true;
		BoardSprite txt6 = new BoardSprite("texto", 7, 1, 7, 1, satk);
		txt6.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt6.text = true;
		BoardSprite txt7 = new BoardSprite("texto", 7, 3, 7, 3, sdef);
		txt7.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt7.text = true;
		BoardSprite txt8 = new BoardSprite("texto", 7, 5, 7, 5, spd);
		txt8.font = new Font("Comic Sans MS", Font.BOLD, 12);
		txt8.text = true;
		
		ArrayList<BoardSprite> hps3 = new ArrayList<>();
		hps3.add(s);
		hps3.add(poke);
		hps3.add(s2);
		hps3.add(txt);
		hps3.add(s3);
		hps3.add(txt2);
		hps3.add(txt3);
		hps3.add(txt4);
		hps3.add(txt5);
		hps3.add(txt6);
		hps3.add(txt7);
		hps3.add(txt8);
		
		JuegoPokemon.t.setBoardSprites(hps3);
		JuegoPokemon.t.draw(map, lmap);
		
		while(JuegoPokemon.f.getCurrentKeyPressed()!='i') {
			
			Thread.sleep(150);
			
			if (JuegoPokemon.f.getCurrentKeyPressed()=='o') {
				retorno = true;
				JuegoPokemon.allyHp = team.hp;
				System.out.println("cambio");
			}
			
			JuegoPokemon.t.draw(map, lmap);
			
			Thread.sleep(150);
			
		}
		
		return retorno;
		
	}

}
