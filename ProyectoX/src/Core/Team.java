package Core;

import java.io.Serializable;

public class Team extends Pokemon implements Serializable {

	int lvl;
	int exp;
	String path2;
	String[] ataques = new String[4];

	public Team(int maxhp, int hp, int atk, int def, int satk, int sdef, int spd, int lvl, int exp, String name,
			String path, String path2, String ataque1, String ataque2, String ataque3, String ataque4) {
		super(maxhp, hp, atk, def, satk, sdef, spd, name, path);
		this.lvl = lvl;
		this.exp = exp;
		this.path2 = path2;
		this.ataques[0] = ataque1;
		this.ataques[1] = ataque2;
		this.ataques[2] = ataque3;
		this.ataques[3] = ataque4;
	}
	/**
	 * This method levels up and strengthens a pokemon.
	 */
	public void lvlUp() {
		if (this.exp>=100) {
			this.exp -= 100;
			this.lvl++;
			this.atk++;
			this.def++;
			this.maxhp += 10;
			this.satk++;
			this.sdef++;
			this.spd++;
		}
	}

}
