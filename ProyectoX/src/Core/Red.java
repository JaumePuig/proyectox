package Core;

import java.io.Serializable;

public class Red extends Characters implements Serializable {

	public Red(int x, int y) {
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	

	/**
	 * Method to move the main character.
	 * 
	 * @param c     char that tells the direction in which to move.
	 * 
	 * @param fight boolean that activates with a wild battle.
	 * @param mapa 2D array which is the map where the character moves in.
	 */
	public boolean redMove(char c, boolean fight, int[][] mapa) {

		if (c == 'a') {
			mapa[this.y][this.x] = 0;
			this.x--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 66;
				fight = true;
			} else {
				this.x++;
				mapa[this.y][this.x] = 66;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 'd') {
			mapa[this.y][this.x] = 0;
			this.x++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 67;
				fight = true;
			} else {
				this.x--;
				mapa[this.y][this.x] = 67;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 's') {
			mapa[this.y][this.x] = 0;
			this.y++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 1;
				fight = true;
			} else {
				this.y--;
				mapa[this.y][this.x] = 1;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 'w') {
			mapa[this.y][this.x] = 0;
			this.y--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 65;
				fight = true;
			} else {
				this.y++;
				mapa[this.y][this.x] = 65;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		}

		return fight;
	}

	/**
	 * Method to move the main character indoor.
	 * 
	 * @param c    char that tells the direction in which to move.
	 * 
	 * @param mapa 2D array which is the map where the character moves in.
	 */
	public void redMoveIndoor(char c, int[][] mapa) {

		if (c == 'a') {
			mapa[this.y][this.x] = 0;
			this.x--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 30;
			} else {
				this.x++;
				mapa[this.y][this.x] = 30;
				JuegoPokemon.f.playSFX("wall.wav");
			}
		} else if (c == 'd') {
			mapa[this.y][this.x] = 0;
			this.x++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 31;
			} else {
				this.x--;
				mapa[this.y][this.x] = 31;
				JuegoPokemon.f.playSFX("wall.wav");
			}
		} else if (c == 's') {
			mapa[this.y][this.x] = 0;
			this.y++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 28;
			} else {
				this.y--;
				mapa[this.y][this.x] = 28;
				JuegoPokemon.f.playSFX("wall.wav");
			}
		} else if (c == 'w') {
			mapa[this.y][this.x] = 0;
			this.y--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 29;
			} else {
				this.y++;
				mapa[this.y][this.x] = 29;
				JuegoPokemon.f.playSFX("wall.wav");
			}
		}

	}

	/**
	 * Method to move the main character in the route.
	 * 
	 * @param c     char that tells the direction in which to move.
	 * 
	 * @param fight boolean that activates with a wild battle.
	 * @param mapa 2D array which is the map where the character moves in.
	 */
	public boolean redMoveRuta(char c, boolean fight, int[][] mapa) {

		if (c == 'a') {
			mapa[this.y][this.x] = 0;
			this.x--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 3;
				fight = true;
			} else {
				this.x++;
				mapa[this.y][this.x] = 3;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 'd') {
			mapa[this.y][this.x] = 0;
			this.x++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 4;
				fight = true;
			} else {
				this.x--;
				mapa[this.y][this.x] = 4;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 's') {
			mapa[this.y][this.x] = 0;
			this.y++;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 1;
				fight = true;
			} else {
				this.y--;
				mapa[this.y][this.x] = 1;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		} else if (c == 'w') {
			mapa[this.y][this.x] = 0;
			this.y--;
			if (mapa[this.y][this.x] == 0) {
				mapa[this.y][this.x] = 2;
				fight = true;
			} else {
				this.y++;
				mapa[this.y][this.x] = 2;
				JuegoPokemon.f.playSFX("wall.wav");
				fight = true;
			}
		}

		return fight;
	}

}
