package Core;

import java.util.Date;
import java.util.GregorianCalendar;

public class Rank implements Comparable{

	String nPLayer;
	int vencidos;
	String calen;
	
	public Rank(String nPLayer, String vencidos2) {
		super();
		this.nPLayer = nPLayer;
		this.vencidos = Integer.parseInt(vencidos2);
		this.calen = new Date().toString();
	}

	
	
	public Rank(String nPLayer, int vencidos, String calen) {
		super();
		this.nPLayer = nPLayer;
		this.vencidos = vencidos;
		this.calen = calen;
	}

	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		
		Rank otro = (Rank) o;
		
		if(this.vencidos>otro.vencidos) {
			return -1;
		}else if(this.vencidos<otro.vencidos) {
			return 1;
		}else {
			return 0;
		}
	}
	
	
	
}
