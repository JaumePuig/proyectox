package Core;

import java.util.ArrayList;

public class Item {
	/**
	 * String with the item's name.
	 */
	String name;
	/**
	 * String with the item's explanation.
	 */
	String comment;
	/**
	 * List with all the items.
	 */
	static ArrayList<Item> itemList = new ArrayList<Item>();
	
	
	public Item(String name, String comment) {
		super();
		this.name = name;
		this.comment = comment;
	}



	/**
	 * Method used to put all the existing items in the itemList.
	 */
	public static void lista() {
		// TODO Auto-generated method stub
		
		itemList.add(new Item("PokeBall","Sirve para atrapar pokemones"));
		itemList.add(new Item("Poción","Cura 20ps a un pokemon"));
		
		
	}
	
}
