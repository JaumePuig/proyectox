package Core;

import java.awt.Font;
import java.io.IOException;
import java.util.ArrayList;

public class mapas {

	/**
	 * One of the main matrixes, representing all scenery objects that wont interact directly with the player's character.
	 */
	public static int[][] fons = { { 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 6, 5, 5, 5, 6, 5, 6, 5, 6, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 6, 5, 5, 5, 5, 5, 5, 61, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 6, 5, 5, 5, 73, 70, 74, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 6, 5, 5, 5, 5, 6, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 6, 5, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 6, 5, 6, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 5, 5, 5, 6, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 8, 6, 5, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 8, 5, 5, 5, 5 },
			{ 5, 5, 73, 70, 74, 6, 5, 5, 5, 71, 69, 72, 5, 5, 5, 73, 70, 74, 5, 5, 5 },
			{ 5, 5, 71, 69, 72, 6, 5, 6, 5, 71, 69, 72, 5, 5, 5, 71, 69, 72, 5, 5, 5 },
			{ 5, 5, 71, 69, 69, 70, 70, 70, 70, 69, 69, 69, 70, 70, 70, 69, 69, 72, 5, 5, 5 },
			{ 5, 5, 71, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 69, 72, 5, 5, 5 },
			{ 5, 5, 83, 84, 84, 84, 84, 84, 84, 69, 69, 69, 84, 84, 84, 84, 84, 85, 5, 5, 5 },
			{ 5, 5, 6, 6, 6, 5, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 5, 5, 6, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 6, 5, 5, 6, 5, 6, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 68, 68, 68, 68, 68, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 68, 68, 68, 68, 68, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 68, 68, 68, 68, 68, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 68, 68, 68, 68, 68, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 68, 68, 68, 68, 68, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 },
			{ 5, 5, 5, 5, 5, 5, 5, 5, 5, 71, 69, 72, 5, 5, 5, 5, 5, 5, 5, 5, 5 } };
	
	/**
	 * One of the main matrixes, representing all scenery objects that interact directly with the player's character and where the main character moves in.
	 */
	public static int[][] player = { { 2, 2, 2, 2, 2, 2, 2, 23, 24, 25, 26, 27, 28, 29, 2, 2, 2, 2, 2, 2, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 30, 31, 32, 33, 34, 35, 36, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 75, 76, 76, 77, 0, 37, 38, 39, 40, 41, 42, 43, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 78, 3, 3, 81, 0, 44, 45, 46, 47, 48, 49, 50, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 78, 0, 4, 81, 0, 51, 52, 53, 54, 55, 56, 57, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 79, 76, 76, 80, 82, 58, 59, 60, 0, 62, 63, 64, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 90, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 19, 20, 21, 22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 19, 20, 21, 22, 0, 2 },
			{ 2, 0, 15, 16, 17, 18, 0, 0, 0, 0, 0, 0, 0, 0, 0, 15, 16, 17, 18, 0, 2 },
			{ 2, 0, 11, 12, 13, 14, 0, 0, 0, 0, 0, 0, 0, 0, 0, 11, 12, 13, 14, 0, 2 },
			{ 2, 0, 7, 0, 9, 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 7, 0, 9, 10, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2 },
			{ 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2 } };
	
	/**
	 * Array combining the two overworld main matrixes.
	 */
	static int[][][] juego = { fons, player };
	
	/**
	 * Array specifying the main matrixes types.
	 */
	static char[] tipos = { 's', 's' };
	
	/**
	 * The main matrix used in a battle.
	 */
	static int[][] combate = { { 0, 2 }, { 1, 0 }, { 0, 0 } };
	static int[][] combateAt = { { 0, 3 }, { 1, 0 }, { 0, 0 } };
	/**
	 * The background matrix used in the houses.
	 */
	static int[][] fondoCasa = {{32,32,32,32,32,32,32,32,32,32,32,32,32},
			{32,3,4,4,4,4,4,4,4,4,4,5,32},
			{32,6,7,7,7,7,7,7,7,7,7,8,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,1,2,1,2,1,2,1,2,1,2,32},
			{32,2,13,0,0,14,1,15,16,24,17,2,32},
			{32,2,13,0,0,14,1,18,19,19,20,2,32},
			{32,2,1,2,1,2,1,21,25,22,23,2,32},
			{32,2,1,2,1,2,26,27,1,2,1,2,32},
			{32,32,32,32,32,32,32,32,32,32,32,32,32}};
	/**
	 * The matrix where the main character moves used in the houses.
	 */
	static int[][] casa = {{32,32,32,32,32,32,32,32,32,32,32,32,32},
			{32,3,4,4,4,4,4,4,4,4,4,5,32},
			{32,6,7,7,7,7,7,7,7,7,7,8,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,9,10,0,0,0,0,0,0,0,32},
			{32,0,0,11,12,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,0,0,0,0,0,0,0,0,0,0,0,32},
			{32,32,32,32,32,32,32,32,32,32,32,32,32}};
	/**
	 * Array combining the two house main matrixes.
	 */
	static int[][][] casas = {fondoCasa, casa};
	/**
	 * Method used when entering a house.
	 * 
	 * @param red Red object that enters the house.
	 * @param team 
	 * @param team 
	 * @param pokeGratis 
	 * @param nPlayer 
	 * @param lider 
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void entrarCasa(Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer, int lider) throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub
		JuegoPokemon.t.setSprites(CasaProves.tileInterior);
		
		JuegoPokemon.t.setColorbackground(0x000000);
		
		red.x=6;
		red.y=10;
		
		JuegoPokemon.t.draw(casas, tipos);
		
		casa(red, team, pokeGratis, nPlayer, lider);
		
		casa[red.y][red.x] = 0;
		
		
	}
	public static void casa(Red red, ArrayList<Team> team, Boolean pokeGratis, String nPlayer, int lider) throws InterruptedException, IOException, ClassNotFoundException {
		casa[red.y][red.x] = 29;
		
		Thread.sleep(120);
		
		boolean salir = false;
		
		
		while (!salir) {
			
			JuegoPokemon.t.draw(casas, tipos);
			
			char c = JuegoPokemon.f.getCurrentKeyPressed();
			
			red.redMoveIndoor(c, casa);
			
			if(fondoCasa[red.y][red.x]==26 && casa[red.y][red.x]==28) {
				salir=true;
			}
			if(fondoCasa[red.y][red.x]==27 && casa[red.y][red.x]==28) {
				salir=true;
			}
			if(JuegoPokemon.f.getCurrentKeyPressed()=='p') {
				Menu.menu(3, red, team, pokeGratis, nPlayer, lider);
			}
			
			
			Thread.sleep(120);
			
		}
	}
	/**
	 * Method that shows a message.
	 */
	public static void cartel() throws InterruptedException {
		if (JuegoPokemon.f.getCurrentKeyPressed() == 'o') {

			String mess = "Pokemon de origen desconocido, se cree que";
			String mess2 = "es originario de una región aún por descubrir";

			BoardSprite s = new BoardSprite("TextBox", 8, 1, 16, 19, "textbox.png");
			BoardSprite txt2 = new BoardSprite("texto", 12, 3, 15, 15, mess);
			txt2.font = new Font("Comic Sans MS", Font.BOLD, 15);
			txt2.text = true;
			BoardSprite txt3 = new BoardSprite("texto", 13, 3, 15, 15, mess2);
			txt3.font = new Font("Comic Sans MS", Font.BOLD, 15);
			txt3.text = true;
			ArrayList<BoardSprite> cart = new ArrayList<>();
			cart.add(s);
			cart.add(txt2);
			cart.add(txt3);
			JuegoPokemon.t.setBoardSprites(cart);

			JuegoPokemon.t.draw(juego, tipos);

			while (JuegoPokemon.f.getCurrentKeyPressed() != 'i') {
				Thread.sleep(50);
			}

		}
	}
}
