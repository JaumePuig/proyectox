package Core;

import java.awt.Font;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Terrible version of a Pokemon game (right now).
 * 
 * @author Jaume Puig
 * @version 0.6
 *
 */

/*
 * 
 * CONTROLES: O -> confirmar I -> salir W -> arriba A -> izquierda S -> abajo D
 * -> derecha
 * 
 */

public class JuegoPokemon implements Serializable {

	/**
	 * Int marking the main character's position in the matrix.
	 */
	static int x = 7;
	/**
	 * Int marking the main character's position in the matrix.
	 */
	static int y = 10;
	/**
	 * New board object to use a graphic interface.
	 */
	static Board t = new Board();
	/**
	 * New window object to use a graphic interface.
	 */
	static Window f = new Window(t);
	/**
	 * New pokemon object.
	 */
	static Team nidorino = new Team(100, 100, 47, 29, 33, 32, 36, 1, 0, "Nidorino", "BackNidorino.gif", "FrontNidorino.png", "Picotazo",
			"Puya nociva", "Cornada", "Terremoto");
	/**
	 * New pokemon object.
	 */
	static Pokemon gengar = new Pokemon(100, 100, 47, 29, 33, 32, 36, "Gengar", "FrontGengar.gif");
	/**
	 * Boolean which will end the game when the player dies.
	 */
	static boolean alive = true;
	/**
	 * Boolean used to decide the music.
	 */
	static boolean music = true;
	static int vencidos = 0;
	static String nPlayer;
	static Villager npc = new Villager(13, 13);
	static Villager oak = new Villager(6, 10);
	static Scanner sc = new Scanner(System.in);
	static boolean cured = false;
	static int lidel = 0;
	static ArrayList<Team> team = new ArrayList<Team>();
	static ArrayList<Integer> HPs = new ArrayList<Integer>();
	/**
	 * Int with the main character's pokemon max hp.
	 */
	static int allyHp = nidorino.maxhp;
	static String[] imatges = { "", "RedGif.gif", "Tree.png", "rock1.png", "octorok.gif", "Hierba1.png", "Hierba2.png",
			"casa30.png", "casa31.png", "casa32.png", "casa33.png", "casa20.png", "casa21.png", "casa22.png",
			"casa23.png", "casa10.png", "casa11.png", "casa12.png", "casa13.png", "casa00.png", "casa01.png",
			"casa02.png", "casa03.png", "lab00.png", "lab01.png", "lab02.png", "lab03.png", "lab04.png",
			"lab05.png", "lab06.png", "lab10.png", "lab11.png", "lab12.png", "lab13.png", "lab14.png", "lab15.png",
			"lab16.png", "lab20.png", "lab21.png", "lab22.png", "lab23.png", "lab24.png", "lab25.png", "lab26.png",
			"lab30.png", "lab31.png", "lab32.png", "lab33.png", "lab34.png", "lab35.png", "lab36.png", "lab40.png",
			"lab41.png", "lab42.png", "lab43.png", "lab44.png", "lab45.png", "lab46.png", "lab50.png", "lab51.png",
			"lab52.png", "puertaLab.png", "lab54.png", "lab55.png", "lab56.png", "RedGifBack.gif", "RedGifLat.gif",
			"RedGifLat2.gif", "tallGrass.png", "centerSand.png", "upperSand.png", "leftSand.png", "rightSand.png",
			"SandEsqUL.png", "SandEsqUR.png", "fence00.png", "fence01.png", "fence02.png", "fence10.png",
			"fence20.png", "fence22.png", "fence12.png", "cartel.png", "Sandbl.png", "Sandbot.png", "Sandbr.png",
			"npc.png", "backnpc.png", "leftnpc.png", "rightnpc.png", "Oak.png" };
	/*
	 * 0:nada 11:casa20 22:casa03 33:lab13 44:lab30 55:lab44 66:RedGifLat 1:red
	 * 12:casa21 23:lab00 34:lab14 45:lab31 56:lab45 67:RedGifLat2 2:arbol 13:casa22
	 * 24:lab01 35:lab15 46:lab32 57:lab46 68:tallGrass 3:roca 14:casa23 25:lab02
	 * 36:lab16 47:lab33 58:lab50 69:centerSand 4:octorok 15:casa10 26:lab03
	 * 37:lab20 48:lab34 59:lab51 70:upperSand 5:hierba1 16:casa11 27:lab04 38:lab21
	 * 49:lab35 60:lab52 71:leftSand 6:hierba2 17:casa12 28:lab05 39:lab22 50:lab36
	 * 61:lab53 72:rightSand 7:casa30 18:casa13 29:lab06 40:lab23 51:lab40 62:lab54
	 * 73:upper-left Sand 8:casa31(puerta) 19:casa00 30:lab10 41:lab24 52:lab41
	 * 63:lab55 74:upper-right Sand 9:casa32 20:casa01 31:lab11 42:lab25 53:lab42
	 * 64:lab56 10:casa33 21:casa02 32:lab12 43:lab26 54:lab43 65:RedGifBack
	 * 75:fence00 76:fence01 77:fence02 78:fence10 79:fence20 80:fence22 81:fence12
	 * 82:cartel 83:sandbl 84:sandbot 85:sandbr 86:npc 87:backnpc 88:leftnpc
	 * 89:rightnpc 90:oak
	 */
	
	public static void main(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
		// TODO Auto-generated method stub

		
		Boolean pokeGratis = false;
		vencidos = 0;
		team.add(nidorino);
		HPs.add(nidorino.maxhp);

		
		
		Item.lista();

		t.setColorbackground(0x51CA75);
		t.setActborder(false);

		t.setSprites(imatges);
		f.setActLabels(false);
		
		f.setTitle("Intento de Pahkiman");
		Red red = new Red(x, y);
		mapas.player[red.y][red.x] = 1;
		mapas.player[npc.x][npc.y] = 86;
		
		nPlayer = f.showInputPopup("Nombre:");

		pueblo(red, team, pokeGratis, nPlayer, lidel);

	}
	
	public static void pueblo(Red red, ArrayList<Team> tean, Boolean pokeGratis, String nPlay, int lider) throws InterruptedException, IOException, ClassNotFoundException {
		
		mapas.player[red.y][red.x] = 1;
		
		nPlayer = nPlay;
		
		team = tean;
		
		lidel = lider;
		
		t.setColorbackground(0x51CA75);
		
		while (alive) {

			ArrayList<BoardSprite> hollow = new ArrayList<>();
			
			t.setBoardSprites(hollow);

			t.setActimgbackground(false);

			t.setColorbackground(0x51CA75);

			if (music) {
				f.playMusic("initialtown.wav");
				music = false;
			}
			t.setSprites(imatges);

			t.draw(mapas.juego, mapas.tipos);

			boolean fight = false;

			char c = f.getCurrentKeyPressed();

			fight = red.redMove(c, fight, mapas.player);

			npc.npcTalk(npc, "Acho, nos vamos al bar?", mapas.player, t, f, false);
			
			if (pokeGratis) {
				cured = oak.npcTalk(npc, "Ale, curado", mapas.player, t, f, true);
			}else {
				pokeGratis = oak.giveItem();
			}
			
			if (cured) {
				allyHp = nidorino.maxhp;
				cured = false;
			}

			npc.npcMove(npc);

			if(f.getCurrentKeyPressed()=='p') {
				Menu.menu(1, red, team, pokeGratis, nPlayer, lider);
				lider = lidel;
			}
			
			if(f.getCurrentKeyPressed()=='t') {
				Thread.sleep(150);
				mapas.player[red.y][red.x] = 0;
				Partida.cargar();
				Thread.sleep(150);
			}
			
			if (mapas.player[6][6] == 65) {
				mapas.cartel();
			}

			if (mapas.fons[red.y][red.x] == 68) {

				if (fight) {
					if (encuentro() == 1) {
						f.playMusic("wildBattle.wav");
						allyHp = Combate.combatInit(t, f, allyHp, nidorino, alive, 1, pokeGratis, team, lider);
//						HPs.set(lider, Combate.combatInit(t, f, HPs.get(lider), nidorino, alive, 1, pokeGratis, team, lider));
						music = true;
						
					}
				}

			}

			if (mapas.fons[red.y][red.x] == 8) {

				int tempx = red.x;
				int tempy = red.y;

				Thread.sleep(120);

				mapas.player[red.y][red.x] = 0;

				mapas.entrarCasa(red, team, pokeGratis, nPlayer, lider);

				red.x = tempx;
				red.y = tempy + 1;
				mapas.player[red.y][red.x] = 1;

			}

			if ((red.y == 25 && red.x == 10) || (red.y == 25 && red.x == 9) || (red.y == 25 && red.x == 11)) {

				int tempx = red.x;
				int tempy = red.y;

				Thread.sleep(120);

				mapas.player[red.y][red.x] = 0;

				Rutas.entrarRuta(red, team, pokeGratis, nPlayer, lider);

				red.x = tempx;
				red.y = tempy - 1;
				mapas.player[red.y][red.x] = 65;
				f.playMusic("initialtown.wav");
				music = false;

			}

			Thread.sleep(120);
		}
		
	}

	/**
	 * Randomly decides if there's a battle with a wild pokemon.
	 */

	public static int encuentro() {
		// TODO Auto-generated method stub

		Random r = new Random();

		int a = r.nextInt(10);

		return a;

	}

}
